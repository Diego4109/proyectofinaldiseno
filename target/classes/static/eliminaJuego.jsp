<%@ include file="cabeza.jsp" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.Juego" %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.JuegoDAODerbyImp" %>
<%@ page import="mx.uaemex.fi.diseno.flyn.generico.NrdaControllerAbstract" %>
<%@ page import="java.sql.*, javax.sql.*, javax.naming.*"%>
<%@ page import="java.util.Hashtable,java.sql.*,javax.naming.*,javax.sql.*" %>

<!-- THE LAYERS�LL BE BROKEN!! -->
<%
	Connection con;
	DataSource ds;
	JuegoDAODerbyImp daoJuegoReal;
	daoJuegoReal = new JuegoDAODerbyImp();

	try {
		InitialContext cxt = new InitialContext();
		ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/ds");
		if(ds==null) {
			throw new ServletException("No encontre la fuente");
		}
	} catch (NamingException e) {
		throw new ServletException(e);
	}
	
	con = ds.getConnection();
	daoJuegoReal.setConexion(con);
	
	ArrayList<Juego> gamesNames = daoJuegoReal.get();
	//ArrayList<Juego> gamesNames = (ArrayList<Juego>)session.getAttribute("AllGames");
	int gamesNumber = gamesNames.size();
%>

<div class="container">
	<h1>Eliminar Juegoo</h1>
	<div class="row 200%">
		<div class="6u 12u$(large)">
			<form method="post" action="eliminaJuegoo">
				<fieldset>
					<legend>Lista de Juegos</legend>
					<div ><table  width="800">
						<thead><tr><th><h4>No.</h4></th><th width="30%" ><h4>Nombre del Juego</h4></th><th width="40%"><h4>Descripci�n</h4></th><th width="30%" ><h4>Url</h4></th><th><h4>Versi�n</h4></th><th><h4>Disponibilidad</h4></th></tr></thead>
						<tbody>
							<%for(int i=0;i<gamesNumber;i++){ %>
								<%String nameS = "availability"+i; %>
								<%String currentName = gamesNames.get(i).getGame();%>
								<%String currentDesc = gamesNames.get(i).getDescripcion();%>
								<%String currentUrl = gamesNames.get(i).getUrl();%>
								<%String currentVersion = gamesNames.get(i).getVersion();%>
								<%String labelVersion = "label"+i;%>
								<tr><td><%=i+1%></td><td><%=currentName%></td><td><%=currentDesc%></td><td><%=currentUrl%></td><td><%=currentVersion%></td><td><select name="<%=nameS%>"><option value="1">Disponible</option><option value="<%=currentName%>">Eliminar</option></select></td></tr>
								<input name="<%=labelVersion%>" type="hidden" value="<%=currentVersion%>">
							<%} %>
						</tbody>
						</table></div>
  				</fieldset>
  				<div class="12u$">
	  				<ul class="actions">	
  						<li><input type="Submit" value="Borrar"></li>
  					</ul>
  				</div>
				<input name="gamesNumber" type="hidden" value="<%=gamesNumber%>">
			</form>
		</div>
	</div>
</div>
<%@ include file="pie.jsp" %>