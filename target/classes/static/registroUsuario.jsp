<%@ include file="cabezaNoSession.jsp" %>
<div class="container">
	<h1>Registro de Usuario</h1>
	<div class="row 200%">
		<div class="6u 12u$(medium)">
			<form method="post" action="registrar">
				<div class="row uniform">
					<div class="6u 12u$(xsmall)">
						<input name="login" id="login" type="text" value="" placeholder="Usuario">
					</div>
					<div class="6u$ 12u$(xsmall)">
						<input name="passwd" type="password" id="passwd" placeholder="Password">
					</div>
					<div class="12u$">
						<div class="select-wrapper">
							<label for="tipousr">Tipo de usuario: </label>
							<select name="tipousr" id="tipousr">
								<option value="">- Seleccione uno -</option>
								<option value="Administrador">Administrador</option>
								<option value="Jugador">Jugador</option>
	
							</select>
						</div>
					</div>
			
					<div class="12u$">
						<div class="select-wrapper">
							<label for="tipousr">Pregunta de Seguridad: </label>
								<select name="preseg" id="preseg">
										<option value="">-Seleccione uno-</option>
								<option value="1">Nombre de tu mascota?</option>
								<option value="2">Cantante Favorito?</option>
								<option value="3">Pais favorito?</option>
							
								</select>
						</div>
					</div>
			
					<div class="12u$">
						<label for="tipousr">Respuesta: </label>
						<br>
						<textarea name="respuesta" id="respuesta" placeholder="Escriba aqui ..." rows="6" required></textarea>
					</div>
			
					<div class="12u$">
						<ul class="actions">
							<li><input type="submit" value="Enviar" /></li>
							<li><input type="reset" value="Borrar" class="alt" /></li>
						</ul>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
<%@ include file="pie.jsp" %>