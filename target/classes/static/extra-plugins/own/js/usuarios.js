$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: GL_HOST + "/api/Usuarios/exeEspecial/format/json",
        dataType: 'json',
        crossDomain: true,
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var fila = "<option value='" + data[i].usu + "'>" + data[i].usu + "</option>";
                $('#usua').append(fila);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
});

function llenaComboUsuarios(){
    $.ajax({
        type: "GET",
        url: GL_HOST + "/api/Usuarios/exeEspecial/format/json",
        dataType: 'json',
        crossDomain: true,
        success: function (data) {

             var acum="";
                for(var i=0; i<data.length ;i++){
                    acum+= "<option value='" + data[i].usu + "'>" + data[i].usu + "</option>";  
               }
                $('#usua').fadeIn(1000).html("<option></option>"+acum+"</br>");
            
        },
        error: function (data) {
            console.log(data);
        }
    });

}


function llenaComboUsuariosAlumnos(){
  //LLena el combo con todos los usuarios
  $.ajax({
    type: "GET",
    url: GL_HOST_ANDREA + "/index.php/api/Usuarios_api/alumnosAll/format/json",
    dataType: 'json',
    crossDomain: true,
    success: function (data) {
      console.log(data);
      var acum = "";

      for (var i = 0; i < data.length; i++) {
        acum += "<option id='" + data[i].clave + "' value='" + data[i].clave + "'>" + data[i].nombres+" "+data[i].primer_apellido+ " "+data[i].segundo_apellido + "</option>";
      }
      console.log(acum);
      $('#alumno').html("<option></option>" + acum);
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function llenaComboUsuariosProfesores(){
  //LLena el combo con todos los usuarios
  $.ajax({
    type: "GET",
    url: GL_HOST_ANDREA + "/index.php/api/Usuarios_api/profesoresAll/format/json",
    dataType: 'json',
    crossDomain: true,
    success: function (data) {
      console.log(data);
      var acum = "";

      for (var i = 0; i < data.length; i++) {
        acum += "<option id='" + data[i].clave + "' value='" + data[i].clave + "'>" + data[i].nombres+" "+data[i].primer_apellido+ " "+data[i].segundo_apellido + "</option>";
      }
      console.log(acum);
      $('#profesores').html("<option></option>" + acum);
    },
    error: function (data) {
      console.log(data);
    }
  });
}



$('#usua').change(function () {
    $.ajax({
        type: "GET",
        url: GL_HOST + "/api/Usuarios/consultaUsuarioPorNombre/format/json",
        dataType: 'json',
        data: {name: $(this).val()},
        crossDomain: true,
        success: function (data) {
            $('#nombre').val(data.nom);
            $('#vige').val(data.f1 +' al '+data.f2);
        },
        error: function (data) {
            console.log(data);
        }
    });
});

function elimina_Alumno() {
   if(confirm("Si eliminas este alumno se eliminaran todas las propuestas que haya realizado y las relacion con su tutor. ¿Eliminar?")){

    console.log($('#alumno').val());
    $.ajax({
        type: 'delete',
        url: GL_HOST_ANDREA + "/index.php/api/Usuarios_api/alumno/format/json",
        data:{alumno: $('#alumno').val()}
    }).done(function (resp) { 
      console.log("Se elimino en alumnos_data");
      elimina_U_Alumno();
    }).fail(function (error) { 
        console.log(error);
    });

  }    

    return false;
}



function elimina_U_Alumno() {
    console.log($('#alumno').val()); 

    $.ajax({
        type: 'delete',
        url: GL_HOST_ANDREA + "/index.php/api/Usuarios_api/usuario/format/json",
        data:{cve: $('#alumno').val()}
    }).done(function (resp) { 
           console.log("Se elimino en usuarios");
           getPropuestasTutorado();
      
    }).fail(function (error) { 
        console.log(error);
    });

    return false;
}


//Obtener todas las cves de propuestas de un tutorado


function getPropuestasTutorado(){

 $.ajax({
        type: 'get',
        url: GL_HOST+'/Propuestas_api/propuestas/format/json',
        data:{clave: $('#alumno').val()}
    }).done(function (resp) {
          console.log("Realizo el get de propuestas:" ); 
          console.log(resp);
          if(resp.length>0){
            for(var i=0; i < resp.length; i++) { 
            var propuesta = resp[i].cve;
            console.log("propuesta: "+propuesta);
            eliminaMateriasPorPropuestaA(propuesta);
           }
         }else{
           eliminaRelacionTutorado();
         }
          
        
    }).fail(function (error) { 
        console.log(error);
    });

}


//Eliminar materias_x_propuesta por alumno de acuerdo a la clave de la propuesta
function eliminaMateriasPorPropuestaA(cve) {
    console.log(cve);
    var clave = cve;
    $.ajax({
        type: 'put',
        url: GL_HOST+'/Propuestas_api/eliminaMateriasPorPropuesta/format/json',
        data:{cve: clave}
    }).done(function (resp) { 
            console.log("Se eliminaron las materias_x_propuesta");
            elimina_propuestas_Alumno();
        
    }).fail(function (error) { 
        console.log(error);
    });

    return false;
}

function elimina_propuestas_Alumno() {
  

    $.ajax({
        type: 'put',
        url: GL_HOST+'/Propuestas_api/eliminaPropuestasAlumno/format/json',
        data:{cve: $('#alumno').val()}
    }).done(function (resp) { 
            console.log("Se eliminaron las propuestas");
            eliminaRelacionTutorado();
        
    }).fail(function (error) { 
        console.log(error);
    });

    return false;
}


function eliminaRelacionTutorado() {
    
    $.ajax({
        type: 'put',
        url: GL_HOST+'/Relaciones_api/borrarRelacionTutorado/format/json',
        data:{cve: $('#alumno').val()}
    }).done(function (resp) { 
        
            $('#usuario').val('');
            $('#nCuenta').val('');         
            $('select').val('');
            operationSuccess("Alumno eliminado");
            llenaComboUsuariosAlumnos();
        
    }).fail(function (error) { 
        //si no hay relacion esta bien
        $('#usuario').val('');
            $('#nCuenta').val('');         
            $('select').val('');
            operationSuccess("Alumno eliminado");
            llenaComboUsuariosAlumnos();
        console.log(error);
    });

    return false;
}



function elimina_Profesor() {
   
   var  r =  confirm("Si eliminas este profesor se eliminaran todas las propuestas que haya revisado y las relaciones con los tutorados. ¿Eliminar?");

   console.log(r);
   
   if(r == true){

       console.log($('#profesores').val());
    $.ajax({
        type: 'delete',
        url: GL_HOST_ANDREA + "/index.php/api/Usuarios_api/profesor/format/json",
        data:{profesor: $('#profesores').val()}
    }).done(function (resp) { 
      console.log("Se elimino en profesores_data");
      elimina_U_Profesor();
    }).fail(function (error) { 
        console.log(error);
    });

   }
 

    

    return false;
}



function elimina_U_Profesor() {
    console.log($('#profesores').val()); 

    $.ajax({
        type: 'delete',
        url: GL_HOST_ANDREA + "/index.php/api/Usuarios_api/usuario/format/json",
        data:{cve: $('#profesores').val()}
    }).done(function (resp) { 
        
          getPropuestasTutor();
        
    }).fail(function (error) { 
        console.log(error);
    });

    return false;
}

//Obtener todas las cves de propuestas de un tutor


function getPropuestasTutor(){

 $.ajax({
        type: 'get',
        url: GL_HOST+'/Propuestas_api/propuestasRevisadas/format/json',
        data:{clave: $('#profesores').val()}
    }).done(function (resp) { 
        if (resp.length>0) {
          for(var i=0; i < resp.length; i++) { 
           var propuesta = resp[i].cve;
            eliminaMateriasPorPropuestaP(propuesta);
           }
         }else{
           eliminaRelacionesTutor();
         }
          
        
    }).fail(function (error) { 
        console.log(error);
    });

}


//Eliminar materias_x_propuesta por profesor de acuerdo a la clave de la propuesta
function eliminaMateriasPorPropuestaP(cve) {

    $.ajax({
        type: 'put',
        url: GL_HOST+'/Propuestas_api/eliminaMateriasPorPropuesta/format/json',
        data:{cve: cve}
    }).done(function (resp) { 
        
            elimina_propuestas_Profesor();
        
    }).fail(function (error) { 
        console.log(error);
    });

    return false;
}


function elimina_propuestas_Profesor() {
    console.log($('#profesores').val()); 

    $.ajax({
        type: 'put',
        url: GL_HOST+'/Propuestas_api/eliminaPropuestasProfesor/format/json',
        data:{cve: $('#profesores').val()}
    }).done(function (resp) { 
        
            eliminaRelacionesTutor();
        
    }).fail(function (error) { 
        console.log(error);
    });

    return false;
}


function eliminaRelacionesTutor() {
    console.log($('#profesores').val()); 

    $.ajax({
        type: 'put',
        url: GL_HOST+'/Relaciones_api/borrarRelacionTutor/format/json',
        data:{cve: $('#profesores').val()}
    }).done(function (resp) { 
        
            $('#usuario').val('');
            $('#nEmpleado').val('');         
            $('select').val('');
            operationSuccess("Profesor eliminado");
            llenaComboUsuariosProfesores();
        
    }).fail(function (error) {
        //si no tiene tutorados esta bien
          $('#usuario').val('');
            $('#nEmpleado').val('');         
            $('select').val('');
            operationSuccess("Profesor eliminado");
            llenaComboUsuariosProfesores();
        console.log(error);
    });

    return false;
}

