/*
-----------------------INFORMACION GENERAL-------------------------------------
Nombre:: generico.js
Objetivo:: Tiene funciones genericas que se usaran en la aplicacion.
-----------------------FUNCIONES-------------------------------------
*/
/*
  Obtenemos los valores dinamicamente de un form, especificamente de los
  inputs y selects que tiene.
  Parametros:
  - ideForm:String == id del form, al que se le va a aplicar una extracción.
  Return:
  - text:ObjectJSON ==Objetos con los elemetnos que tiene el form
*/
function readDataForm(ideForm){
  var text = '{';
  ideForm = '#'+ideForm;//Concatenamos, el '#' para su uso en el JQuery
  var ind = 1;
  $(ideForm +' input,'+ ideForm +' select,'+ideForm+' textarea').each(
    function(index){
      var input = $(this);
      text += '"par'+ind+'" : "'+input.val()+'", ';
      ind++;
    }
  );
  text = text.substring(0, text.length - 2) + '}'
  return JSON.parse(text);
}

/*
  Invocamos un servicio para poder leer datos y llenar 'un Datatable'
  Parametros:
  - servicio:String == url del servicio.
  - selectId:String == id del datatable a llenar.
  Return:
  1. data:ObjectJSON == Valores de la tabla.
  2. data:Object == Error.
  */
function readDataTable(servicio, tableId,buton){
   $.ajax({
        type: "GET",
        url: servicio,
        dataType: 'json',
        crossDomain: true,
        success: function (data) {
           var t = $('#'+tableId).DataTable();
            t.rows().remove().draw();
             for (var index = 0; index < data.length; index++) {
                
                  var ele = data[index];
                  t.row.add( [ele.descripcion,buton]).draw( false );
                
              }
        },
        error: function (data) {
            console.log(data);
        }
    });
  return false;
}

/*
  Invocamos un servicio para poder leer datos y llenar 'select'
  Parametros:
  - servicio:String == url del servicio.
  - selectId:String == id del select a llenar.
  Return:
  1. data:ObjectJSON == Valores de la tabla.
  2. data:Object == Error.
  */

function readDataSelect(servicio, selectId){
   $.ajax({
        type: "GET",
        url: servicio,
        dataType: 'json',
        crossDomain: true,
        success: function (data) {
                   var str = "";
            for (var index = 0; index < data.length; index++) {
              var ele = data[index];
              str += "<option value='"+ele.cve+"'>"+ele.descripcion+"</option>"
            }
            $('#'+selectId).append(str);
        },
        error: function (data) {
            console.log("ERROR: ajax food.readData");
            console.log(data);
        }
    });
  return false;
}

/*
  Invocamos un servicio para poder guardar datos
  Parametros:
  - servicio:String == url del servicio
  - vals:ObjectJSON == valores que se van a insertar.
  - e:ObjectEvent == Evento que llama a la funcion.
  Return:
  1. data:ObjectJSON == Numero > 0, indica que se hizo la insercion.
  2. data:Object == Error.
*/
function writeData(servicio, vals, e){
  e.preventDefault();
  $.ajax({
    type: "POST",
    url: GL_HOST + servicio,
    data: vals,
    crossDomain: true
  }).done(function( data ) {
    if (!isNaN(data.clave)) {//Pregunta si el valor es un numero.
      operationSuccess();
      $('#btnlim').trigger( "click" );//ejecutar el boton de limpiar
    }else{
      operationError();
    }
    
  }).fail(function(data) {
    console.log("ERROR: ajax food.writeData");
    console.log(data);
  });
  return false;
}
/*
  Muestra mensaje de que se guardo correctamente el registo, lanza 
  un modal dentro de las vistas que tengan definido la etiqueta:
                    <div id="mensaje"></div>
  Parametros:
  Return:
*/
function operationSuccess(msj){
  //Creamos el modal que queremo mostrar en la vista.
  var mod = '<div id="modal_exito" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
  mod += '<div class="modal-dialog modal-sm" role="document">';
  mod += '<div class="modal-content" style="border-radius: 10px !important;">';
  mod += '<div class="modal-header" style="background: #008d4c;">';
    mod += '<h4 class="modal-title"><b style="color: white;font-size: large;">CORRECTO</b></h4>';
  mod += '</div>';
  mod += '<div class="modal-body" style="background: #00a65a;">';
    mod += '<p style="color: white;font-size: medium;">'+msj+'</p>';//Mensaje que se va a mostrar
  mod += '</div>';
  mod += '</div>';
  mod += '</div>';
  mod += '</div>';
  //Vaciamos el div
  $('#mensaje').empty();
  //Cargamos el nuevo modal dentro del DIV
  $('#mensaje').html(mod);
  //Lanzamos el modal cargado para enviarle el mensaje al usuario.
  $('#modal_exito').modal('show');
}
/*
  Muestra mensaje de que NO se guardo correctamente el registo, lanza 
  un modal dentro de las vistas que tengan definido la etiqueta:
                    <div id="mensaje"></div>
  Parametros:
  Return:
*/
function operationError(){
  //Creamos el modal que queremo mostrar en la vista.
  var mod = '<div id="modal_exito" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
  mod += '<div class="modal-dialog modal-sm" role="document">';
  mod += '<div class="modal-content" style="border-radius: 10px  !important;">';
  mod += '<div class="modal-header" style="background: #D33724;">';
    mod += '<h4 class="modal-title"><b style="color: white;font-size: large;" >ERROR</b></h4>';
  mod += '</div>';
  mod += '<div class="modal-body" style="background: #dd4b39;">';
    mod += '<p style="color: white;font-size: medium;">La operación fallo. Intentalo más tarde.</p>';//Mensaje que se va a mostrar
  mod += '</div>';
  mod += '</div>';
  mod += '</div>';
  mod += '</div>';
  //Vaciamos el div
  $('#mensaje').empty();
  //Cargamos el nuevo modal dentro del DIV
  $('#mensaje').html(mod);
  //Lanzamos el modal cargado para enviarle el mensaje al usuario.
  $('#modal_exito').modal('show');
}
/*
  Muestra mensaje de que NO se guardo correctamente el registo, lanza 
  un modal dentro de las vistas que tengan definido la etiqueta:
                    <div id="mensaje"></div>
  Parametros:
  Return:
*/
function operationAdvertencia(msg){
  //Creamos el modal que queremo mostrar en la vista.
  var mod = '<div id="modal_exito" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
  mod += '<div class="modal-dialog modal-sm" role="document">';
  mod += '<div class="modal-content" style="border-radius: 10px  !important;">';
  mod += '<div class="modal-header" style="background: #db8b0b;">';
    mod += '<h4 class="modal-title"><b style="color: white; font-size: large;" >ADVERTENCIA</b></h4>';
  mod += '</div>';
  mod += '<div class="modal-body" style="background: #f39c12;">';
    mod += '<p style="color: white;font-size: medium;">'+ msg +'</p>';//Mensaje que se va a mostrar
  mod += '</div>';
  mod += '</div>';
  mod += '</div>';
  mod += '</div>';
  //Vaciamos el div
  $('#mensaje').empty();
  //Cargamos el nuevo modal dentro del DIV
  $('#mensaje').html(mod);
  //Lanzamos el modal cargado para enviarle el mensaje al usuario.
  $('#modal_exito').modal('show');
}
/*ALERT
  Parametros:
  -msg:String == Mensaje que mostrata el alert.
  -tipo:Int == tipo del alerta que se mostrara.
    (1: success, 2: warning, 4: info, 5: error)

    COMO USAR?
    - Crear un mensaje dentro de un elemento DIV que tenga un id="mensaje"
     <div id="mensaje"></div>

*/
function alerta(tipo,msg) {
  var tipoClase = "";
  var str = "";

  switch(tipo){
    case 1://success
      tipoClase = "alert-success";
      break;
    case 2://warning
      tipoClase = "alert-warning";
      break;
    case 3://info
      tipoClase = "alert-info";
      break;
    case 4://error
      tipoClase = "alert-danger";
      break;                  
  }
  str += '<div class="alert '+tipoClase+' alert-dismissible">';
  str += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
  str += '<h4><i class="icon fa fa-check"></i></h4>';
  str += msg;
  str += '</div>';
  //Vaciamos el div
  $('#mensaje').empty();
  //
  //Cargamos el nuevo modal dentro del DIV
  $('#mensaje').append(str);
}

/*
  Eliminamos un registro de la tabla, que esta en la base de datos que cumple
  con la clave.
  Parametros:
  - svc:String == Servicio que se quiere invocar.
  - datos:Object == El objeto contiene los valores necesarios para poder ejecutar el servicio.
  Return:
*/
function eliminar(svc, datos, event){
  $.ajax({
    type: "DELETE",
    url: GL_HOST + svc,
    data: datos,
    crossDomain: true
  }).done(function( data ) {
    console.log('# '+ data);
    switch (data) {
      case 777:
        alerta(1, 'Tu registro se eliminó correctamente.');
        break;
      case 800:
        alerta(2, 'Tu registro está siendo usado.');
        break;
      case 666:
        alerta(4, 'ERROR');
        break;
    }
  }).fail(function(error) {
    console.log("ERROR: ajax generico.getPagina");
    console.log(error);
  });
  return false;
}