<%@ include file="cabeza.jsp" %>
<div class="container">
	<h1>Juegos Eliminados</h1>
	<div class="row 200%">
		<div class="6u 12u$(medium)">
			<form method="post" action="inicial.jsp">
					<fieldset>
						<legend>Los siguientes juegos fueron eliminados con �xito</legend>
						<%for(int i=0;i<(Integer)request.getAttribute("noOfEliminatedGames");i++){ %>
							<%String aux = (String)request.getAttribute("gameEliminated"+i); %>
							<h5><%=aux%></h5>
							<%} %>
  						<ul class="actions">	
  							<li><input type="Submit" value="Aceptar"></li>
  						</ul>
  					</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="pie.jsp" %>