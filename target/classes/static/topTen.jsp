<%@ include file="cabeza.jsp" %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.Usuario"  %>
<%@ page import="mx.uaemex.fi.diseno.flyns.model.data.Records"  %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.Juego"  %>
<%@ page import="java.util.ArrayList" %>
<!-- Banner -->

<style>

table th,td {
  text-align: center;
}

</style>

	<section id="top">
	<div class="container">
	<h1>TopTen de <% out.print((String) session.getAttribute("juegoSelected")); %></h1>
	<div class="row 200%">
		<div class="6u 12u$(large)">
				<fieldset>
					<legend>Lista de Jugadores</legend>
	
		
	
		<div>
			
	
			<table style="width:100%;">
				<thead>
					<th><h4>#</h4>></th>
					<th><h4>Jugador</h4></th>
					<th><h4>Puntaje</h4></th>
					<th><h4>Fecha</h4></th>
				</thead>
				<tbody>
					<%
					int j=0;
					boolean invitacion = true;
					ArrayList<Records> topten = (ArrayList<Records>) session.getAttribute("topten");
					if(topten.size()>0){
					
						for(int i=0;i<topten.size();i++){
							out.print("<tr>");
							
							out.print("<td>");
							out.print(i+1);
							out.print("</td>");
							
							out.print("<td>");
							out.print(topten.get(i).getJugador().getLogin());
							out.print("</td>");
							
							out.print("<td>");
							out.print(topten.get(i).getRecord());
							out.print("</td>");
							
							out.print("<td>");
							out.print(topten.get(i).getDate());
							out.print("</td>");
							
							out.print("</tr>");
							j++;
							if(topten.get(i).getJugador().getLogin().compareTo(u.getLogin())==0){
								invitacion=false;
							}
						}					
					}else{
						out.print("<tr aling='center'><td aling='center' colspan='4'>No hay records disponibles para este juego</td></tr>");
					}
					if(j<10){
						if(invitacion){
							out.print("<tr aling='center'><td aling='center' colspan='4'>Aun no esta lleno el top ten. Deberias intentarlo :)</td></tr>");
						}
					}
					%>
				</tbody>
			</table>
	
		</div>
		</fieldset>
		</div>
		</div>
		</div>
		
	
	</section>	

<%@ include file="pie.jsp" %>