<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.Usuario"  %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.TipoUsuario"  %>
<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Flynns Arcade</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="inicial.jsp">Flynn's Arcade <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					<li><a href="inicial.jsp">Inicial</a></li>
					<li><a href="generic.html">Generic</a></li>
					<li><a href="elements.html">Elements</a></li>
					<%  
							Usuario u = (Usuario) session.getAttribute("Usuario");
							if(u==null){
								response.sendRedirect("index.jsp");
							} else{
								if(u.getTipo().getTipo().compareTo("Administrador")==0){
									out.print("<li><a href='registraJuego.jsp'>Registrar juego</a></li><li><a href='eliminaJuego.jsp'>Eliminar juego</a></li>");
								}else if(u.getTipo().getTipo().compareTo("Jugador")==0){
									out.print("<li><form method='post' action='PotroRecord' id='record'><a id='verJuegos'>RECORDS</a></form></li>");
								}else{
									out.print("<li><a href=''>"+u.getTipo().getTipo()+"</a></li>");
								}
							}
							
								
						%>
					<li>
							<a onclick='cierraSession()'>Cerrar sesion</a>
					
					</li>
				</ul>
			</nav>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Colecci&oacute;n de Juegos</p>
						<h2>UAEM</h2>
					</header>
				</div>
			</section>
			
			<script>
			
			document.getElementById("verJuegos").onclick = function() {
			    document.getElementById("record").submit();
			}
			function cierraSession(){
				  $.ajax({
		                type : 'PUT',
		                url : 'http://localhost:8080/FlynsConCapas/abreteSesamo',
		                contentType: 'application/json',
		                success : function(data, status, xhr){
		                   window.location.replace("http://localhost:8080/FlynsConCapas/index.jsp");
		                },
		                error: function(xhr, status, error){
		                    window.location.replace("http://localhost:8080/FlynsConCapas/index.jsp");
				               console.log(error);
		                }
		            });
			}
			
			</script>