<%@ include file="cabeza.jsp" %>
<div class="container">
	<h1>Registrar Juego</h1>
	<div class="row 200%">
		<div class="6u 12u$(medium)">
			<form method="post" action="registraJuego">
					<fieldset>
						<legend>Datos del Juego</legend>
						<div class="6u 12u$(xsmall)">
							<p><input type="text" name="gameName"  placeholder="Nombre del Juego" required></p>
						</div>
						<div class="6u 12u$(xsmall)">
							<p><input  type="text" name="gameUrl" placeholder="Url del Juego" required="" style="width: 275px;"></p>
						</div>
						<div class="6u 12u$(xsmall)">
							<p><input type="number" name="gameVers" min="1" max="100" size="10"  style="width: 160px" placeholder="Versi�n del juego" required></p>
						</div>
						<div class="6u 12u$(xsmall)">
	  						<p><textarea name="gameDesc" rows="3" cols="100" placeholder="Descripci�n del juego" required="" style="width: 553px; height: 79px;"></textarea></p>
	  					</div>
  					</fieldset>
  					<div class="12u$">
  						<ul class="actions">	
  							<li><input type="Submit" value="Enviar"></li>
  							<li><input type="reset" value="Borrar" class="alt"></li>
  						</ul>
  					</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="pie.jsp" %>