package mx.uaemex.fi.diseno.flyns.principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.uaemex.fi.diseno.flyns.principal.juegos.JuegoServicio;

@Controller
@RequestMapping("arcade/Paginas")
public class Pagina {
	
	private JuegoServicio juegoServicio;
	@Autowired
	public Pagina(JuegoServicio elServicio) {
		this.juegoServicio = elServicio;
	}

	public Pagina() {
	}

	//========INDEX===============
	@GetMapping("/bienvenido")
//	@ResponseBody
	public String bienvenido(Model model) {
		model.addAttribute("mensaje", "hola");
		return "index";
	}
	//===========pagina principal================
	@GetMapping("/inicial")
//	@ResponseBody
	public String inicial(Model model,HttpServletRequest request) {
	
		
		model.addAttribute("usuario", request.getSession().getAttribute("Usuario"));

		
		return "inicial";
	}
//=================================================JUEGOS =========================================
	@GetMapping("/registraJuego")
//	@ResponseBody
	public String registraJuego(Model model,HttpServletRequest request) {
		model.addAttribute("usuario", request.getSession().getAttribute("Usuario"));

		return "juego/registraJuegoV";
	}
	
	@GetMapping("/eliminaJuego")
//	@ResponseBody
	public String eliminaJuego(Model model,HttpServletRequest request) {
		List<Map<String, Object>> response = juegoServicio.getAllGames();
		for(int i =0; i<response.size();i++) {
			System.out.println("Salio: "+response.get(i).get("JUEGO"));
		}
		model.addAttribute("juegos", response);
		model.addAttribute("usuario", request.getSession().getAttribute("Usuario"));
		//Get all the games in the DB
		return "/juego/eliminaJuego";
	}
//=================================================JUEGOS =========================================
	
	
	//================Usuarios===================
	@GetMapping("/registraUsuario")
//	@ResponseBody
	public String registraUsuario(Model model,HttpServletRequest request) {
		return "usuario/registroUsuario";
	}
	
	@GetMapping("/verRecords")
//	@ResponseBody
	public String verJuegos(Model model,HttpServletRequest request) {
		model.addAttribute("usuario", request.getSession().getAttribute("Usuario"));

		return "juego/verJuegos";
	}
	
	@GetMapping("/howmany")
//	@ResponseBody
	public String burgers(Model model,HttpServletRequest request) {
		model.addAttribute("usuario", request.getSession().getAttribute("Usuario"));
		return "juego/burgers";
	}
}
