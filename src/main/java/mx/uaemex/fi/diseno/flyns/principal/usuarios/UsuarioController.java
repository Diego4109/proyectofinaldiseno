package mx.uaemex.fi.diseno.flyns.principal.usuarios;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.uaemex.fi.diseno.flyns.model.data.Juego;
import mx.uaemex.fi.diseno.flyns.model.data.JuegoData;
import mx.uaemex.fi.diseno.flyns.model.data.LoginData;
import mx.uaemex.fi.diseno.flyns.model.data.Records;
import mx.uaemex.fi.diseno.flyns.model.data.Usuario;
import mx.uaemex.fi.diseno.flyns.model.data.topData;
import mx.uaemex.fi.diseno.flyns.principal.juegos.JuegoServicio;

@RestController
@RequestMapping("arcade/api/Usuarios")
public class UsuarioController {
	
	private UsuarioServicio usuarioServicio;
	private JuegoServicio juegoServicio;

	@Autowired
	public UsuarioController(UsuarioServicio elServicio,JuegoServicio temp) {
		this.usuarioServicio = elServicio;
		this.juegoServicio = temp;
	}
	

	@PostMapping("/validarAcceso")
	@ResponseBody
	public ResponseEntity<Integer> iniciaSesion(@RequestBody LoginData data,HttpServletRequest request){
		Usuario encontrado = usuarioServicio.obtiene(data.getLogin(), data.getPassword());
		if(encontrado == null) {
			return ResponseEntity.ok(745);
		}
		HttpSession session;
		session = request.getSession();
		session.setAttribute("Usuario", encontrado);
		
			return ResponseEntity.ok(777);
	}
	
	@PutMapping("/cerrarSesion")
	@ResponseBody
	public ResponseEntity<Integer> cierraSesion(HttpServletRequest request){
		HttpSession session;
		session = request.getSession();
		session.invalidate();
		return ResponseEntity.ok(777); 
	}
	
	@PostMapping("/agrega")
	@ResponseBody
	public ResponseEntity<Usuario> registrar(@RequestBody Usuario data){
		
		Usuario saved = usuarioServicio.salva(data.getLogin(),data.getPassword(),data.getPregunta(),data.getRespuesta());
		System.out.println(saved);
		if(saved == null) {
			return ResponseEntity.status(500).build();
		}
		return ResponseEntity.ok(saved);
	}
	
	@GetMapping("/todos")
	@ResponseBody
	public ResponseEntity<ArrayList<Juego>> obtenerTodos(HttpServletRequest request){
		
		ArrayList<Juego> juegos = usuarioServicio.getAll(); 
		if(juegos == null) {
			return null;
		}
	
		return ResponseEntity.ok(juegos);
	}

	@GetMapping("/record/{juego}")
	@ResponseBody
	public ResponseEntity<Records> obtenerRecord(@PathVariable String juego,HttpServletRequest request){
		
		
		Usuario u = (Usuario) request.getSession().getAttribute("Usuario");
		Records rec = this.usuarioServicio.getRecord(u, juego);
		if(rec==null) {
			return ResponseEntity.ok(new Records());
		}
	
		return ResponseEntity.ok(rec);
	}
	
	@GetMapping("/topten/{id}")
	@ResponseBody
	public ResponseEntity<ArrayList<Records>> obtenerTopten(@PathVariable int id,HttpServletRequest request){
		
		ArrayList<Records> rec = this.usuarioServicio.getTopten(id);
		if(rec==null) {
			return ResponseEntity.ok(new ArrayList<Records>());
		}
	
		return ResponseEntity.ok(rec);
	}
}
