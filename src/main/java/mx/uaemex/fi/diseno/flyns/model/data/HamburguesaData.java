package mx.uaemex.fi.diseno.flyns.model.data;

public class HamburguesaData {
	
	private int record;
	public int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public HamburguesaData() {
		
	}
	
	public int getRecord() {
		return record;
	}
	public void setRecord(int record) {
		this.record = record;
	}
}
