package mx.uaemex.fi.diseno.flyns.principal.juegos;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import mx.uaemex.fi.diseno.flyns.model.data.Juego;

@Component
public class JuegoRepository {
	private static final Log log = LogFactory.getLog(JuegoRepository.class);
	
	private DataSource dataSource;
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public Juego searchByName(String gameName) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		String searchSQL = "SELECT * FROM JUEGOS WHERE JUEGO = '"+gameName+"' AND ACTIVO = TRUE";
		try {
			List<Map<String, Object>> response = this.jdbcTemplate.queryForList(searchSQL, params);
			if(response.size() == 0)
				return null;
			Juego gameResponse = buildGameUsingList(response.get(0));
			return gameResponse;
		} catch (DataAccessException e) {
			log.error("Fail to save :(",e);
			return null;
		}
	}
	
	public void insertGame(Juego j) {
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource(); 
		params.addValue("juego", j.getNombre());
		params.addValue("activo", j.isActivo());
		params.addValue("url", j.getUrl());
		params.addValue("descripcion", j.getDesc());
		params.addValue("version", j.getVers());
		
		String insertSQL = "INSERT INTO juegos (juego,activo,url, descripcion, version) VALUES(:juego,:activo,:url,:descripcion,:version)";
		try {
			this.jdbcTemplate.update(insertSQL, params, holder);
		} catch (DataAccessException e) {
			log.error("Fail to safe :(",e);
		}
	}


	private Juego buildGameUsingList(Map<String, Object> map) {
		boolean flag;
		if(map.get("ACTIVO").toString().toUpperCase().equals("FALSE")) {
			flag = false;
		}else {
			flag = true;
		}
		return new Juego(map.get("JUEGO").toString(), flag, map.get("URL").toString(), map.get("DESCRIPCION").toString(), map.get("VERSION").toString());
	}


	public Juego salvaJuego(Juego j) {
		log.info("Juego salvado: "+ j.getNombre()); // info por 
		
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource(); 
		params.addValue("juego", j.getNombre());
		params.addValue("activo", j.isActivo());
		params.addValue("url", j.getUrl());
		
		String insertSQL = "INSERT INTO juegos (juego,activo,url) VALUES(:juego,:activo,:url)";
		try {
			this.jdbcTemplate.update(insertSQL, params, holder);
		} catch (DataAccessException e) {
			log.error("Fail to safe :(",e);
			return null;
		}
		return new Juego(holder.getKey().intValue(),j.getNombre(),j.isActivo(),j.getUrl());
		
	}
	@Autowired
	public JuegoRepository(DataSource ds) {
		this.dataSource = ds;
		this.jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
	}

	public void deleteGame(Juego j) {
		MapSqlParameterSource params = new MapSqlParameterSource(); 
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		params.addValue("juego", j.getNombre());
		params.addValue("activo", j.isActivo());
		params.addValue("url", j.getUrl());
		params.addValue("descripcion", j.getDesc());
		params.addValue("version", j.getVers());
		params.addValue("id", j.getId());
		String updateSQL = "UPDATE juegos SET activo= false WHERE JUEGO = :juego AND VERSION = :version";
		try {
			this.jdbcTemplate.update(updateSQL, params, holder);
		} catch (DataAccessException e) {
			log.error("Fail to safe :(",e);
		}
	}
	
	public List<Map<String, Object>> getAllGames() {
		MapSqlParameterSource params = new MapSqlParameterSource();
		String searchSQL = "SELECT * FROM JUEGOS WHERE ACTIVO = TRUE";
		try {
			List<Map<String, Object>> response = this.jdbcTemplate.queryForList(searchSQL, params);
			if(response.size() == 0)
				return null;
			return response;
		} catch (DataAccessException e) {
			log.error("Fail to save :(",e);
			return null;
		}
	}

}
