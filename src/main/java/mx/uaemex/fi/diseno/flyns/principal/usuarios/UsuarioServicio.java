package mx.uaemex.fi.diseno.flyns.principal.usuarios;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.uaemex.fi.diseno.flyns.model.data.Juego;
import mx.uaemex.fi.diseno.flyns.model.data.Records;
import mx.uaemex.fi.diseno.flyns.model.data.Usuario;
import mx.uaemex.fi.diseno.flyns.principal.juegos.JuegoRepository;

@Component
public class UsuarioServicio {
	
	private UsuarioRepository repositorio;

	@Autowired
	public UsuarioServicio(UsuarioRepository r) {
		this.repositorio = r;
	}
	
	public Usuario obtiene(String login,String password) {
		return this.repositorio.get(new Usuario(login,password));
	}
	
	public Usuario salva(String login,String password,int pregunta,String respuesta) {
		System.out.println("service");
		//System.out.println(this.repositorio.get(new Usuario(login,password,2,0,pregunta,respuesta)));
		return this.repositorio.salvausuario(new Usuario(login,password,2,0,pregunta,respuesta));
	}
	
	public ArrayList<Juego> getAll(){
		return this.repositorio.getAll();
	}
	
	public Records getRecord(Usuario u,String juego) {
		return this.repositorio.getRecords(u, juego);
	}
	
	public ArrayList<Records> getTopten(int juego) {
		return this.repositorio.getTopten(juego);
	}
}
