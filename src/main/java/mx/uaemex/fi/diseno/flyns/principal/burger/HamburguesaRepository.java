package mx.uaemex.fi.diseno.flyns.principal.burger;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import mx.uaemex.fi.diseno.flyns.model.data.Hamburguesa;

@Component
public class HamburguesaRepository {
	
private static final Log log = LogFactory.getLog(HamburguesaRepository.class);
	
	private DataSource dataSource;
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public Hamburguesa salvaRecord(Hamburguesa j) {
		Hamburguesa hm = consultaRecord(j);
		boolean molly=false;
		if(j.getRecord()>hm.getRecord()) {
			molly = true;
		}
		int idHowmanyburgers=3;
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		String d=""+formatter.format(date);
		log.info("Record salvado: "+ j.getId()+";" +j.getRecord()); // info por 
		
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource(); 
		params.addValue("record", j.getRecord());
		params.addValue("jugador", j.getId());
		params.addValue("juego", idHowmanyburgers);
		params.addValue("fecha", d);
		
		if(molly) {
			String insertSQL = "UPDATE records SET record= :record "
					+ " WHERE jugador= :jugador AND juego= :juego";
			try {
				this.jdbcTemplate.update(insertSQL, params, holder);
			} catch (DataAccessException e) {
				log.error("Fallo update :(",e);
				return null;
			}
		}else {
			String insertSQL = "INSERT INTO records (record,jugador,juego,fecha) "
					+ "VALUES(:record,:jugador,:juego,:fecha)";
				try {
					this.jdbcTemplate.update(insertSQL, params, holder);
				} catch (DataAccessException e) {
					log.error("Fail to safe :(",e);
					return null;
				}
		}
		return new Hamburguesa(holder.getKey().intValue(),j.id,d);
		
	}
	
	
	public Hamburguesa consultaRecord(Hamburguesa j) {
		Hamburguesa m= new Hamburguesa();
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource(); 
		params.addValue("jugador", j.getId());
		String selectSQL = "SELECT record FROM records"
				+" WHERE records.jugador ="+j.getId();
		try { 
			m.setRecord(this.jdbcTemplate.queryForObject(selectSQL, params, Integer.class));
			//m= this.jdbcTemplate.queryForObject(selectSQL,params, new RecordRowMapper());
			//this.jdbcTemplate.update(insertSQL, params, holder);
		} catch (DataAccessException e) {
			log.error("Select fallo :(",e);
			return null;
		}
		return m;
		
	}
	
	class RecordRowMapper implements RowMapper<Hamburguesa>{
		@Override
		public Hamburguesa mapRow(ResultSet rs, int i) throws SQLException{
			Hamburguesa p= new Hamburguesa();
			p.setId(rs.getInt("jugador"));
			p.setRecord(rs.getInt("record"));
			return p;
		}
	}
	@Autowired
	public HamburguesaRepository(DataSource ds) {
		this.dataSource = ds;
		this.jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
	}
}
