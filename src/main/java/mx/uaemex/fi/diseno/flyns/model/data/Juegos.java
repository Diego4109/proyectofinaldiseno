package mx.uaemex.fi.diseno.flyns.model.data;

import java.util.ArrayList;

public class Juegos {
	 private ArrayList<Juego> juegos;

	public ArrayList<Juego> getJuegos() {
		return juegos;
	}

	public void setJuegos(ArrayList<Juego> juegos) {
		this.juegos = juegos;
	}
}
