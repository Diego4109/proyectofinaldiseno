package mx.uaemex.fi.diseno.flyns.principal.usuarios;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import mx.uaemex.fi.diseno.flyns.model.data.Juego;
import mx.uaemex.fi.diseno.flyns.model.data.Records;
import mx.uaemex.fi.diseno.flyns.model.data.Usuario;
import mx.uaemex.fi.diseno.flyns.principal.juegos.JuegoRepository;

@Component
public class UsuarioRepository {
	private static final Log log = LogFactory.getLog(JuegoRepository.class);
	
	private DataSource dataSource;
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	public UsuarioRepository(DataSource ds) {
		this.dataSource = ds;
		this.jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
	}
	
	public Usuario get(Usuario usuario) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		String searchSQL = "SELECT * FROM usuarios where nombre=:nombre and password=:password";
		params.addValue("nombre", usuario.getLogin());
		params.addValue("password", usuario.getPassword());
		try {
			List<Map<String, Object>> response = this.jdbcTemplate.queryForList(searchSQL, params);
			//System.out.println("RESPUESTA: "+response.get(0).get("JUEGO"));
			if(response.size() == 0)
				return null;
			Usuario usResponse = buildUsuarioUsingList(response.get(0));
			return usResponse;
		} catch (DataAccessException e) {
			log.error("Fail to get :(",e);
			return null;
		}
	}
	

	public Usuario salvausuario(Usuario u) {
		log.info("Juego salvado: "+ u.getLogin()); // info por 
		
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		MapSqlParameterSource params = new MapSqlParameterSource(); 
		
		params.addValue("nombre", u.getLogin());
		params.addValue("password", u.getPassword());
		params.addValue("tipo", 2);
		params.addValue("pregunta",u.getPregunta() );
		params.addValue("respuesta", u.getRespuesta());
		
		String insertSQL = "INSERT INTO usuarios (nombre,password,tipo,pregunta,respuesta) VALUES(:nombre,:password,:tipo,:pregunta,:respuesta)";
		try {
			this.jdbcTemplate.update(insertSQL, params, holder);
		} catch (DataAccessException e) {
			log.error("Fail to safe  us:(",e);
			return null;
		}
		return new Usuario(u.getLogin(),u.getPassword(),2,holder.getKey().intValue(),u.getPregunta(),u.getPassword());
	}
	
	private Usuario buildUsuarioUsingList(Map<String, Object> map) {
		return new Usuario(map.get("NOMBRE").toString(), map.get("PASSWORD").toString(), Integer.parseInt(map.get("TIPO").toString()), Integer.parseInt(map.get("ID").toString()),Integer.parseInt(map.get("PREGUNTA").toString()), map.get("RESPUESTA").toString());
	}
	

	public ArrayList<Juego> getAll() {
		MapSqlParameterSource params = new MapSqlParameterSource();
		String searchSQL = "SELECT * FROM juegos where activo=true";
		try {
			List<Map<String, Object>> response = this.jdbcTemplate.queryForList(searchSQL, params);
			//System.out.println("RESPUESTA: "+response.get(0).get("JUEGO"));
			if(response.size() == 0)
				return null;
			ArrayList<Juego> responseList = new ArrayList<Juego>();
			for(int i=0;i<response.size();i++) {
				responseList.add(buildGameUsingList(response.get(i)));
			}
			
			return responseList;
		} catch (DataAccessException e) {
			log.error("Fail to get :(",e);
			return null;
		}
	}
	private Juego buildGameUsingList(Map<String, Object> map) {
		boolean flag;
		if(map.get("ACTIVO").toString().toUpperCase().equals("FALSE")) {
			flag = false;
		}else {
			flag = true;
		}
		return new Juego(map.get("JUEGO").toString(), flag, map.get("URL").toString(), map.get("DESCRIPCION").toString(), map.get("VERSION").toString(),Integer.parseInt((map.get("ID").toString())));
	}
	
	public Records getRecords(Usuario u,String j) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		String searchSQL = "SELECT r.record, r.fecha, u.nombre, j.juego"
				+ " FROM records r, usuarios u, juegos j"
				+ " WHERE r.jugador=u.id AND r.juego=j.id "
				+ " AND j.activo=true AND u.nombre='"+u.getLogin()+"' AND j.juego='"+j+"'";
		System.out.println(searchSQL);
		try {
			List<Map<String, Object>> response = this.jdbcTemplate.queryForList(searchSQL, params);
			if(response.size() == 0)
				return null;
			Records usResponse = buildRecordsUsingList(response.get(0));
			return usResponse;
		} catch (DataAccessException e) {
			log.error("Fail to get record:(",e);
			return null;
		}
	}
	
	private Records buildRecordsUsingList(Map<String, Object> map) {
		return new Records(map.get("NOMBRE").toString(), Integer.parseInt(map.get("RECORD").toString()), map.get("JUEGO").toString(),map.get("FECHA").toString());
	}
	
	public ArrayList<Records> getTopten(int j) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		String sql="SELECT r.record, r.fecha, u.nombre, j.juego"
				+ " FROM records r, usuarios u, juegos j"
				+ " WHERE r.jugador=u.id AND r.juego=j.id "
				+ " AND j.activo=true AND j.id="+j+" "
				+ " GROUP BY record, fecha, nombre, j.juego"
				+ " ORDER BY r.record DESC "
				+ " FETCH FIRST 10 ROWS ONLY";
		System.out.println(sql);
		try {
			List<Map<String, Object>> response = this.jdbcTemplate.queryForList(sql, params);
			if(response.size() == 0)
				return null;
			
			ArrayList<Records> rs = new ArrayList<Records>();
			for(int i=0;i<response.size();i++) {
				Records usResponse = buildRecordsUsingList(response.get(i));
				rs.add(usResponse);
			}
			return rs;
		} catch (DataAccessException e) {
			log.error("Fail to get record:(",e);
			return null;
		}
	}

}
