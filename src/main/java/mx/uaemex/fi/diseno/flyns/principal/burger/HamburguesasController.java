package mx.uaemex.fi.diseno.flyns.principal.burger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.uaemex.fi.diseno.flyns.model.data.Hamburguesa;
import mx.uaemex.fi.diseno.flyns.model.data.Usuario;
import mx.uaemex.fi.diseno.flyns.model.data.HamburguesaData;

@Controller
@RequestMapping("arcade/api/howmany")
public class HamburguesasController {
	private HamburguesaServicio hamburguesaServicio;
	
	@Autowired
	public HamburguesasController(HamburguesaServicio elServicio) {
		this.hamburguesaServicio = elServicio;
	}
	
	
	@PostMapping("/burgers")
	@ResponseBody
	public ResponseEntity<Hamburguesa> salvaHamburguesa(@RequestBody HamburguesaData data,HttpServletRequest request){
		
		Usuario u = (Usuario) request.getSession().getAttribute("Usuario");
		
		Hamburguesa saved = hamburguesaServicio.salvaRecord(u.getId(), data.getRecord());
		if(saved == null) {
			return ResponseEntity.status(500).build();
		}
		return ResponseEntity.ok(saved);
	}

}
