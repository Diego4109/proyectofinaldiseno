package mx.uaemex.fi.diseno.flyns.principal.juegos;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.uaemex.fi.diseno.flyns.model.data.Juego;

@Component
public class JuegoServicio {
	
	private JuegoRepository repositorio;
	
	@Autowired
	public JuegoServicio(JuegoRepository r) {
		this.repositorio = r;
	}
	
	public Juego salva(String n,String url) {
		return this.repositorio.salvaJuego(new Juego(n,url));
	}
	
	public Juego searchByName(String gameName) {
		return this.repositorio.searchByName(gameName);
	}
	
	public void insertGame(Juego gameToInsert) {
		this.repositorio.insertGame(gameToInsert);
	}
	
	public void deleteGame(Juego gameToDelet) {
		this.repositorio.deleteGame(gameToDelet);
	}
	
	public List<Map<String, Object>> getAllGames() {
		return this.repositorio.getAllGames();
	}

}
