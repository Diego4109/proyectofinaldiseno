package mx.uaemex.fi.diseno.flyns.model.data;


public class TipoUsuario implements Data{
	private String tipo;
	private int id;
	
	public TipoUsuario() {
		
	}
	
	public int getId() {
		return this.id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setId(int id) {
		this.id = id;
	}
}

