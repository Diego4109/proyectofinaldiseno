package mx.uaemex.fi.diseno.flyns.config;

import java.util.Arrays;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import mx.uaemex.fi.diseno.flyns.filtro.Fisgon;


@Configuration //Conesta esta anotacion lo hacemos un configurardor
@ComponentScan("mx.uaemex.fi.diseno.flyns") //Donde buscar componentes con

public class AppConfig {

	public AppConfig() {
		// TODO Auto-generated constructor stub
	}
/*	@Bean
	public JuegoRepository juegoRepository() {// el nombre del metodo servicos 
		return new JuegoRepository();
	}
	@Bean
	public JuegoServicio juegoServicio() {
		return new JuegoServicio(juegoRepository());
	}
*/
		@Bean
		public FilterRegistrationBean<Fisgon> auditingFilterRegistrationBean(){
			FilterRegistrationBean<Fisgon> registro = new FilterRegistrationBean<>();
			Fisgon filtro = new Fisgon();
			registro.setFilter(filtro);
			registro.setOrder(Integer.MAX_VALUE);
			registro.setUrlPatterns(Arrays.asList("/messages/*"));
			return registro;
			
		}
		
}
