package mx.uaemex.fi.diseno.flyns.principal.juegos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import mx.uaemex.fi.diseno.flyns.model.data.Juego;
import mx.uaemex.fi.diseno.flyns.model.data.JuegoData;

@Controller
@RequestMapping("/juegos")
public class JuegosController {
	private JuegoServicio juegoServicio;
	
	@Autowired
	public JuegosController(JuegoServicio elServicio) {
		this.juegoServicio = elServicio;
	}
	
	@PostMapping("/agrega")
	@ResponseBody
	public ResponseEntity<Juego> salvaJuego(@RequestBody JuegoData data){
		Juego saved = juegoServicio.salva(data.getNombre(), data.getUrl());
		if(saved == null) {
			return ResponseEntity.status(500).build();
		}
		return ResponseEntity.ok(saved);
	}
	
	@PostMapping("/eliminaJuegosC")
	public String eliminaJuegosC(@RequestParam(value="id", required=false) String id,@RequestParam(value="nombre", required=false) String nombre,@RequestParam(value="version", required=false) String vers,Model model){
		
		String[] ids = id.split(",");
		String[] nombres = nombre.split(",");
		String[] versiones = vers.split(",");
		
		List<String> data = new ArrayList<String>();
		
		for(int i=0; i<ids.length;i++) {
			if(ids[i].equals("1")) {
				//No se elimina el registro
			}else { //Eliminar en esa posición
				//Crear el objeto jueg con esos parametros
				juegoServicio.deleteGame(new Juego(nombres[i], versiones[i]));
				data.add(nombres[i]);
				//System.out.println("SE ELIMINARAN LOS: "+nombres[i]);
			}
		}
		 model.addAttribute("mois",data);
		 
		return "juego/deletedGamesDoneV";
	}
	
	@PostMapping("/registraJuegoC")
	public String registraJuegoC(@RequestParam(value="gameName", required=false) String gameName, 
			@RequestParam(value="gameUrl", required=true) String gameUrl,
			@RequestParam(value="gameVers", required=true)String gameVers,
			@RequestParam(value="gameDesc", required=true)String gameDesc, Model model) {
		//Set the parameters for the game object
		gameName = gameName.toUpperCase();
		gameVers = gameVers+".0";
		//Looking for that game into de DB using the name
		Juego response = juegoServicio.searchByName(gameName);
		if(response==null) { //Different name (There´s no game in the DB)
			juegoServicio.insertGame(new Juego(gameName, true, gameUrl, gameDesc, gameVers));
			model.addAttribute("gameName", gameName);
			model.addAttribute("gameVers", gameVers);
			return "juego/insertedGamesDoneV";
		}else { //There is a game with the same name into de DB and is ACTIVE
			if(response.getVers().equals(gameVers)) { //The version is the same (The same game)
				//Send to view
				model.addAttribute("gameName", gameName);
				model.addAttribute("gameVers", gameVers);
				return "juego/gameAlreadyInDBV";
			}else {//Same name but other version
				//Delet the current game in the DB
				juegoServicio.deleteGame(response);
				//Insert the new game
				juegoServicio.insertGame(new Juego(gameName, true, gameUrl, gameDesc, gameVers));
				model.addAttribute("gameName", gameName);
				model.addAttribute("gameVers", gameVers);
				return "juego/insertedGamesDoneV";
			}
		}
	}

}
