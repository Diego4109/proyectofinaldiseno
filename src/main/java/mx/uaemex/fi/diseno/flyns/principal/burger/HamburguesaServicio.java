package mx.uaemex.fi.diseno.flyns.principal.burger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.uaemex.fi.diseno.flyns.model.data.*;

@Component
public class HamburguesaServicio {
	
private HamburguesaRepository repositorio;
	
	@Autowired
	public HamburguesaServicio(HamburguesaRepository r) {
		this.repositorio = r;
	}
	
	public Hamburguesa salvaRecord(int n,int record) {
		return this.repositorio.salvaRecord(new Hamburguesa(n,record));
	}

}
