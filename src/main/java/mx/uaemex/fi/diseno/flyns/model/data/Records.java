package mx.uaemex.fi.diseno.flyns.model.data;

public class Records {
	private int id;
	private String jugador;
	private int record;
	private String juego;
	private String date;
	
	public String getJugador() {
		return jugador;
	}

	public void setJugador(String jugador) {
		this.jugador = jugador;
	}

	public String getJuego() {
		return juego;
	}

	public void setJuego(String juego) {
		this.juego = juego;
	}	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	


	public int getRecord() {
		return record;
	}

	public void setRecord(int record) {
		this.record = record;
	}

	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	
	public Records() {
		
	}

	public Records(String jugador, int record, String juego, String date) {
		super();
		this.jugador = jugador;
		this.record = record;
		this.juego = juego;
		this.date = date;
	}
	
	

}
