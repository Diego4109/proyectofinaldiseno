package mx.uaemex.fi.diseno.flyns.model.data;

public class Juego {
	private String nombre;
	private boolean activo;
	private String url;
	private Integer id;
	private String desc;
	private String vers;
	public Juego() {
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getVers() {
		return vers;
	}
	public void setVers(String vers) {
		this.vers = vers;
	}
	
	public Juego(String nombre, String vers) {
		this.id = 1;
		this.activo = false;
		this.url = "u";
		this.nombre = nombre;
		this.vers = vers;
	}
	
	public Juego(Integer id,String nombre,boolean ac, String url) {
		this.id = id;
		this.activo = ac;
		this.url = url;
		this.nombre = nombre;
	}
	public Juego(String n) {
		this.nombre = n;
	}
	//CONSTRUCTOR WITH ALL PARAMETERS
	public Juego( String name, boolean activ, String u, String de, String ve) {
		this.nombre = name;
		this.activo = activ;
		this.url = u;
		this.desc = de;
		this.vers = ve;
	}
	
	public Juego( String name,  String u, String de, String ve) {
		this.nombre = name;
		this.url = u;
		this.desc = de;
		this.vers = ve;
	}
	

	public Juego( String name, boolean activ, String u, String de, String ve,Integer id) {
		this.nombre = name;
		this.activo = activ;
		this.url = u;
		this.desc = de;
		this.vers = ve;
		this.id = id;
	}
	/*public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Juego(String n) {
		this.nombre = n;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	public Juego(Integer key,String nombre,boolean ac, String url) {
		this.id = key;
		this.activo = ac;
		this.url = url;
		this.nombre = nombre;
	}
	
	public Juego(String nombre, String url) {
		this.activo = false;
		this.url = url;
		this.nombre = nombre;
	}
*/
}
