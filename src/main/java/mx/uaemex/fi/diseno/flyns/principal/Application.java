package mx.uaemex.fi.diseno.flyns.principal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import mx.uaemex.fi.diseno.flyns.config.AppConfig;

@SpringBootApplication
public class Application {

	public Application() {
}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
	}

}
