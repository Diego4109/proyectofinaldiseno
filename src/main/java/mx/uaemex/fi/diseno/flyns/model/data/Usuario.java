package mx.uaemex.fi.diseno.flyns.model.data;

public class Usuario implements Data{
	
	private String login;
	private String password;
	private int tipo;
	private int id;
	private int pregunta;
	private String respuesta;
	public Usuario() {}
	
	public Usuario(String login,String password) {
		this.login = login;
		this.password = password;
	}
	
	public Usuario(String login,String password,int tipo,int id,int pregunta,String respuesta) {
		this.login = login;
		this.password = password;
		this.tipo = tipo;
		this.id = id;
		this.pregunta = pregunta;
		this.respuesta = respuesta;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPregunta() {
		return pregunta;
	}

	public void setPregunta(int pregunta) {
		this.pregunta = pregunta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
}

