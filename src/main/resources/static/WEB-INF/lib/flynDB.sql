CREATE TABLE tiposUsuarios(
	id INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY (START WITH 5, INCREMENT BY 1),
	nombre varchar(100),
	primary key(id)
);

CREATE TABLE preguntas(
	id INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY (START WITH 5, INCREMENT BY 1),
	pregunta varchar(100),
	primary key(id)
);

CREATE TABLE usuarios(
	id INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY (START WITH 5, INCREMENT BY 1),
	nombre varchar(15),
	password varchar(255),
	tipo integer references tiposUsuarios(id),
	pregunta integer references preguntas(id),
	respuesta varchar(50),
	primary key(id)
);

CREATE TABLE juegos(
	id INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY (START WITH 0, INCREMENT BY 1),   
	juego VARCHAR(25),   
	activo boolean,
	url VARCHAR(50),
	descripcion VARCHAR(50) NOT NULL DEFAULT 'Enjoy',
	version VARCHAR(5) NOT NULL DEFAULT '1.0',
	primary key(id)	
);

CREATE TABLE records
(id INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY (START WITH 0, INCREMENT BY 1),  
	record integer, 
	jugador integer references usuarios(id),     
	juego integer references juegos(id),
	fecha date,
	primary key (id)
);