/*
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu" id="">
            <li><a href="../tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
*/

function crearMenu(){
      var str = '<li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>CÉDULAS</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_2"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>EQUIPO</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_9"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>ESTADÍSTICAS</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_11"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>MATERIAL</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_7"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>NODOS</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_4"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>PARÁMETROS</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_8"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>PERFIL</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_10"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>PROVEEDORES / CLIENTES</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_3"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>RUTAS</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_5"></ul></li><li class="treeview"><a href="#"><i class="fa fa-table"></i> <span>TIPOS DE VEHÍCULOS</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu" id="CAT_6"></ul></li>';
      $('#menuopc').append(str);
      //cargarOpciones();

}

function cargarOpciones(){
  $.ajax({
    type: "GET",
    url: GL_HOST+"/api/Menu/opciones/format/json",
    crossDomain: true
  }).done(function( resp ) {
      for(var index = 0; index < resp.length; index++){
          var opc = resp[index];
          str = '<li><a href="'+GL_HOST+opc.url+'"><i class="fa fa-circle-o"></i>'+opc.dopcion+'</a></li>';
          $('#CAT_'+opc.cmenu+'').append(str);
      } 
  }).fail(function(data) {
    console.log("ERROR: ajax food.writeData");
    console.log(data);
  });    
}