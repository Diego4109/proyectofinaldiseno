/*
------------------------INFORMACION GENERAL-------------------------------------
Nombre:: validar.js
Objetivo:: Validaciones para usar en los form's. SOLo js
----------------------------FUNCIONES-------------------------------------------
*/
/*
Objetivo:: Solicita el id del elemento.
Parametro::
  - elem : String == id del elemento sin el '#'.
  - msj : String == Mensaje a mostrar en caso de que tiene error.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
*/
function msjError(elem, msj){
  $("#"+elem).css("border-color","red");
  $("#"+elem+"-span").remove();
  $("#"+elem).after('<span id="'+elem+'-span" style="color: #dd4b39;">'+msj+'</span>')
}

function msjValido(elem){
  $("#"+elem+"-span").remove();
  $("#"+elem).css("border-color","green");
}
/*
Objetivo: Un elemento es obligatorio.
Parametro::
  - elem : String == id del elemento sin el '#'.
  - msj : String == Mensaje a mostrar en caso de que tiene error.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
 */
function obligatorio(elem) {
    var valor = $('#'+elem).val();
    if( valor == null || valor.length == 0 || /^\s+$/.test(valor) || valor == "none") {
        var msj = "Campo obligatorio."
        msjError(elem, msj);
        return false;//No cumple
    }
    msjValido(elem);
    return true;//si cumple
}

function obligatorioRadio(name,id) {

    var valor = $("input[name='"+name+"']:checked").val();
  
    if( valor == null || valor.length == 0 || /^\s+$/.test(valor) || valor == "none") {
        var msj = "Campo obligatorio."
        msjError(id, msj);
        return false;//No cumple
    }
    msjValido(id);
    return true;//si cumple
}


/*Funcion que valida el select de un combo estado con fines de funcionalidad (Nodos)*/
function obligatorioEst(elem) {
    var valor = $('#'+elem).val();
    if( valor == null || valor.length == 0 || /^\s+$/.test(valor) || valor == "none") {
        var msj = "Campo obligatorio."
        //msjError(elem, msj);
        return false;//No cumple
    }
    msjValido(elem);
    return true;//si cumple
}
/*Funcion que envia el mensaje de que la contraseña del usuario es incorrecta*/
  function obligatorioContraseniaActual(elem) {
    var valor = $('#'+elem).val();
  
        var msj = "Contraseña incorrecta."
        msjError(elem, msj);
        return false;//No cumple
   
}

/*
Objetivo:: solicita el valor y determina que sean 10 numeros de puros numeros.
Patron:: (NNN)NN-NN-NNN => N = [0-9]
Parametro::
  - elem : String == ID del elemento que va a ser evaluado.
  - msj : String == Mensaje a mostrar en caso de que tiene error.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
*/
function telefono(elem) {
    var valor = $('#'+elem).val();
    if( !(/^\(\d{3}\)\d{2}-\d{2}-\d{3}$/.test(valor)) ) {
        var msj = "El número debe tener 10 digitos[0-9]."
        msjError(elem, msj);
          return false;//No cumple
    }
    msjValido(elem);
    return true;
}
/*
Objetivo::
    Solicita el parametro y evalua que cumpla las sintaxis de como debe ser un correo.
Parametro::
    - elem : String == ID del elemento que va a ser evaluado.
    - msj : String == Mensaje a mostrar en caso de que tiene error.  
Return::
    - false : boolean == NOPI pasa la prueba de validacion
    - true : boolean == SIPI, pasa la validacion
*/
function correo(elem) {
    var valor = $('#'+elem).val();
    if( !(/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)/.test(valor)) ) {
        var msj = "El valor no es un correo.";
        msjError(elem, msj);
        return false;//No cumple
    }
    msjValido(elem);
    return true;
}
/*
Objetivo::
Verifica que el valor tiene unicamente numeros.
Parametro::
  - elem : String == ID del elemento que va a ser evaluado.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
*/
function numerico(elem) {
    var valor = $('#'+elem).val();
    if( !(/^\d+$/.test(valor)) ) {
        var msj = "Ingresa solo números";
        msjError(elem, msj);
        return false;//No cumple
    }
    msjValido(elem);
    return true;
}

/*
Objetivo::
Verifica que el valor tiene unicamente letras.
Parametro::
  - elem : String == ID del elemento que va a ser evaluado.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
*/
function letras(elem) {
    var valor = $('#'+elem).val();
  if( !(/^[a-zA-Z]*$/.test(valor)) ) {
        var msj = "El valor deben ser solo letras[a-z o A-Z].";
        msjError(elem, msj);
        return false;//No cumple
    }
    msjValido(elem);
    return true;
}
/*
Objetivo::
Validar que el numero tenga cinco digitos.
Parametro::
  - elem : String == ID del elemento que va a ser evaluado.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
*/
function esCP(elem){
    var valor = $('#'+elem).val();
   if( !(/^[0-9]{5}$/.test(valor)) ) {
        var msj = "El campo debe tener 5 dígitos.[0-9].";
        msjError(elem, msj);
        return false;//No cumple
    }
    msjValido(elem);
    return true;    
}
/*
Objetivo::
    Validar que el valor se un decimal. 
Parametro::
  - elem : String == ID del elemento que va a ser evaluado.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
*/
function esDecimal(elem){
    var valor = $('#'+elem).val();
  if( !(/^((\d+)|(\d+\.\d+))$/.test(valor)) ) {
        var msj = "El campo debe un número entero o decimal.";
        msjError(elem, msj);
        return false;//No cumple
    }
    msjValido(elem);
    return true;    
}

/* 
Se encuentra el numero dentro de un rango.
Parametro::
  - elem : String == ID del elemento que va a ser evaluado.
  - min : int == numero minimo a aceptar.
  - max : int == numero maximo a aceptar.
Return::
  - false : boolean == NOPI pasa la prueba de validacion
  - true : boolean == SIPI, pasa la validacion
*/
function rangoNumerico(elem, min, max){
  var valor = parseFloat($('#'+elem).val());
  if( valor >= min && valor <= max){//Si el valor esta dentro del rango.
    msjValido(elem);
    return true;
  }else{
    var msj = "El valor debe estar entre "+min+" y "+max;
    msjError(elem, msj);
    return false;//No cumple
  } 
}

function rangoMin(elem, min){
  var valor = parseFloat($('#'+elem).val());
  if( valor > min){//Si el valor esta dentro del rango.
    msjValido(elem);
    return true;
  }else{
    var msj = "El valor debe ser mayor a "+min;
    msjError(elem, msj);
    return false;//No cumple
  } 
}
/*Longitud de la palabra */
function lenPalabra(elem, min, max){
  var valor = $('#'+elem).val().length;
  if(max == -1){
    if( valor >= min){//Si el valor esta dentro del rango.
      msjValido(elem);
      return true;
    }else{
      var msj = "El valor debe tener una longitud mínima "+min+".";
      msjError(elem, msj);
      return false;//No cumple
    }     
  }else{
    if( valor >= min && valor <= max){//Si el valor esta dentro del rango.
      msjValido(elem);
      return true;
    }else{
      var msj = "El valor debe estár entre una longitud mínima "+min+" y máxima "+max;
      msjError(elem, msj);
      return false;//No cumple
    }     
  }
}
/* Validacion de nombre de usuario a registrar*/
function esName(elem){
  var valor = $('#'+elem).val();
  if( !(/^[a-zA-Z0-9_ñÑ]*$/.test(valor)) ) {
    var msj = "El campo debe tener letras(a-z o A-Z), números(0-9) y simbólos(_)";
    msjError(elem, msj);
    return false;//No cumple
  }
  msjValido(elem);
  return true;
}

/* */
function esPassword(elem){
  var valor = $('#'+elem).val();
  if( !(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@!?&\-_#])[A-Za-z\d$@!?&\-_#]/.test(valor)) ) {
    var msj = "El campo tiene que contener al menos una (a-z)(A-Z)(0-9) y ($@!?&-_#).";
    msjError(elem, msj);
    return false;//No cumple
  }
  msjValido(elem);
  return true;
}