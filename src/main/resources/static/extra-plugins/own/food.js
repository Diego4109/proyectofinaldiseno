/*
  Obtenemos los valores dinamicamente de un form, especificamente de los
  inputs y selects que tiene.
  Parametros:
  - ideForm:String == id del form, al que se le va a aplicar una extracción.
  Return:
  - text:ObjectJSON ==Objetos con los elemetnos que tiene el form
*/
function readDataForm(ideForm){
  var text = '{';
  ideForm = '#'+ideForm;//Concatenamos, el '#' para su uso en el JQuery
  var ind = 1;
  $(ideForm +' input,'+ ideForm +' select,'+ideForm+' textarea').each(
    function(index){
      var input = $(this);
      text += '"par'+ind+'" : "'+input.val()+'", ';
      ind++;
    }
  );
  text = text.substring(0, text.length - 2) + '}'
  return JSON.parse(text);
}
/*
  Invocamos un servicio para poder leer datos y llenar 'select'
  Parametros:
  - servicio:String == url del servicio.
  - selectId:String == id del select a llenar.
  Return:
  1. data:ObjectJSON == Valores de la tabla.
  2. data:Object == Error.
  */
function readData(servicio, selectId){
  $.ajax({
    type: "GET",
    url: servicio,
    dataType: "json",
    crossDomain: true
  }).done(function( data ) {
    var str = "";
    for (var index = 0; index < data.length; index++) {
      var ele = data[index];
      str += "<option value='"+ele.clave+"'>"+ele.nombre+"</option>"
      console.log(data);
    }
    $('#'+selectId).append(str);
  }).fail(function(data) {
    console.log("ERROR: ajax food.readData");
    console.log(data);
  });
  return false;
}

/*
  Invocamos un servicio para poder guardar datos
  Parametros:
  - servicio:String == url del servicio
  - vals:ObjectJSON == valores que se van a insertar.
  - e:ObjectEvent == Evento que llama a la funcion.
  Return:
  1. data:ObjectJSON == Numero > 0, indica que se hizo la insercion.
  2. data:Object == Error.
*/
function writeData(servicio, vals, e){
  e.preventDefault();
  $.ajax({
    type: "POST",
    url: servicio,
    data: vals,
    crossDomain: true
  }).done(function( data ) {
    $('#btnlim').trigger( "click" );
    alert( "Registro guardado.");
  }).fail(function(data) {
    console.log("ERROR: ajax food.writeData");
    console.log(data);
  });
  return false;
}
