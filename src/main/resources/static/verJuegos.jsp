<%@ include file="cabeza.jsp" %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.Usuario"  %>
<%@ page import="mx.uaemex.fi.diseno.flyns.model.data.Records"  %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.Juego"  %>
<%@ page import="java.util.ArrayList" %>

<style>

table th,td {
  text-align: center;
}

</style>

<!-- Banner -->
	<section id="main">
	<div class="container">
	<h1>Consulta de Records</h1>
	<div class="row 200%">
		<div class="6u 12u$(large)">
				<fieldset>
					<legend>Lista de Juegos</legend>
	<div>
	<table style="width:100%;">
		<thead>
			<th><h4>Juego<h4></th>
			<th></th>
			<th></th>
		</thead>
		<tbody>
			<%
				ArrayList<Records> records = (ArrayList<Records>) session.getAttribute("records");
				ArrayList<Juego> juegos = (ArrayList<Juego>)session.getAttribute("Juegos");
				if(juegos.size()!=0){
					for(int i=0;i<juegos.size();i++){
						out.print("<tr>");
						out.print("<td>");
						out.print(juegos.get(i).getGame());
						out.print("</td>");
						out.print("<td>");
						out.print("<button id='"+juegos.get(i).getGame()+"' name='"+records.get(i).getDate()+"' value='"+records.get(i).getRecord()+"' onclick='muestraRecord(this.id,this.name,this.value)'>consultar mi record</button>");
						out.print("</td>");
						out.print("<td>");
						out.print("<form method='get' action='PotroRecord'>");
						out.print("<input type='text' name='juego' value='"+juegos.get(i).getGame()+"' style='	display: none'>");
						out.print("<input type='submit' value='consultar top ten'>");
						out.print("</form>");
						out.print("</td>");
						out.print("</tr>");
					}
				}else{
					out.print("<tr aling='center'><td colspan='3'>No hay juegos disponibles</td></tr>");
				}
				
			%>
		</tbody>
	</table>
	
	</div>
	</fieldset>
	</div>
	</div>
	</div>

	</section>
	
	<script>
		function muestraRecord(juego,fecha,record){
			if(fecha!='null'){
				bootbox.alert({
				    message: "Tu record en "+juego+" es:"+record+". Fecha:"+fecha,
				    className: 'rubberBand animated'
				});
			}else{
				bootbox.alert({
				    message: "Aun no has jugado este juego.",
				    className: 'rubberBand animated'
				});
			}
		}
		
	</script>

<%@ include file="pie.jsp" %>